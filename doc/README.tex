\input title
\input keywords

\setvariables
  [this]
  [title={The Real-Time 3D Lattice Boltzmann Method Using OpenGL and OpenCL},
   author={Alexander Shukaev, Viacheslav Mikerov},
   keyword={computational fluid dynamics, Lattice Boltzmann method, lid-driven
            cavity, channel, OpenCL, OpenGL, parallelization, visualization,
            density field, velocity field, particle tracing, inflow, outflow,
            no-slip, free slip, arbitrary geometries}]

\setupinteraction
  [state=start,
   title={\getvariable{this}{title}},
   author={\getvariable{this}{author}},
   keyword={\getvariable{this}{keyword}}]

\setuppapersize
  [A4]
  [A4]

\setupbodyfont
  [12pt]

\setupwhitespace
  [big]

\setuppagenumbering
  [location={footer, middle},
   style=bold]

\setuphead
  [subject]
  [style=\bfa]

\setupcaption
  [figure]
  [numberstyle=\bf,
   way=bysection,
   numbersection=yes,
   prefix=yes,
   prefixsegments=chapter:section]

\setupenumerations
  [before={\blank[big]},
   after={\blank[big]},
   alternative=serried,
   width=broad,
   distance=0.5em,
   headstyle=\bf,
   titlestyle=\bf,
   way=bysection,
   numbersection=yes,
   prefix=yes,
   prefixsegments=chapter:section,
   conversion=numbers]

\definereferenceformat
  [figure]
  [text={Figure }]

\Title{\getvariable{this}{title}}
\Author{\getvariable{this}{author}}

\placebookmarks
  [subject]

\starttext

\placetitle

\startkeywords

\getvariable{this}{keyword}

\stopkeywords

% ------------------------------------------------------------------------------

\useURL
  [url:1]
  [http://en.wikipedia.org/wiki/Lattice_Boltzmann_methods]
  []
  [Lattice Boltzmann method (LBM)]

\useURL
  [url:2]
  [http://en.wikipedia.org/wiki/Computational_fluid_dynamics]
  []
  [computational fluid dynamics (CFD)]

\useURL
  [url:3]
  [http://en.wikipedia.org/wiki/OpenGL]
  []
  [OpenGL]

\useURL
  [url:4]
  [http://en.wikipedia.org/wiki/OpenCL]
  []
  [OpenCL]

\useURL
  [url:5]
  [http://en.wikipedia.org/wiki/Graphics_processing_unit]
  []
  [graphics processing unit (GPU)]

\subject
  [description]
  {Description}

\thinrule

This project is an open-source implementation of the 3D \from[url:1], a popular
class of \from[url:2] methods for fluid simulation. The implementation takes
advantage of \from[url:3] to provide high-quality hardware-accelerated
visualization and \from[url:4] to perform heavy computations associated with the
method on \from[url:5]. Although, the algorithm of LBM has many variations, in
general it inherently possesses very good parallelization properties which fit
into the computational model offered by OpenCL specification very well. This
project aims to exploit these properties in order to achieve real-time in both
visualization and computation aspects.

\stopsection

% ------------------------------------------------------------------------------

\subject
  [features]
  {Features}

\thinrule

The following types of scenarios are currently supported by the implementation:

\startitemize[packed]
  \item
    Lid-driven cavity;

  \item
    Inflow/outflow;

  \item
    Arbitrary geometries (obstacles).
\stopitemize

Of course these 3 types can be combined to create arbitrarily complex scenarios.

The following visualization methods are implemented:

\startitemize[packed]
  \item
    Density field mapped into color distribution;

  \item
    Velocity field mapped into vector glyph distribution;

  \item
    Particle tracing in the velocity field.
\stopitemize

\stopsection

% ------------------------------------------------------------------------------

\subject
  [goals]
  {Goals}

\thinrule

The following design goals were prioritized during the development:

\startitemize[packed]
  \item
    High quality and computational efficiency of visualization methods;

  \item
    High performance and scalability of LBM solver;

  \item
    Efficient OpenGL/OpenCL interoperability with minimum bandwidth;

  \item
    Separation of visualization and computation jobs through utilization of
    multi-threading and their smart management to allow run-time adaptive
    workload balance between OpenGL and OpenCL;

  \item
    Introduction of minimal GUI to allow scenario choosing, parameter
    adjustments, and general control of the application workflow;

  \item
    Object-oriented paradigm domination;

  \item
    Preferably cross-platform application;

  \item
    Utilization of bleeding-edge development tools, libraries, specifications,
    standards, etc.
\stopitemize

\stopsection

% ------------------------------------------------------------------------------

\useURL
  [url:6]
  [http://en.wikipedia.org/wiki/C\%2B\%2B]
  []
  [C++]

\useURL
  [url:7]
  [http://en.wikipedia.org/wiki/C\%2B\%2B11]
  []
  [C++11]

\subject
  [implementation]
  {Implementation}

\thinrule

The application is written in \from[url:6], more importantly the \from[url:7]
standard is employed. As suggested above, the project is divided into 2 main
subsystems, which in turn consist of several important classes and routines
listed below:

\startitemize
  \item
    Visualization:

    \startitemize[packed]
      \item
        \type{source/DomainRenderer.hpp} --- domain boundary visualization;

      \item
        \type{source/DensityRenderer.hpp} --- density field visualization;

      \item
        \type{source/VelocityRenderer.hpp} --- velocity field visualization;

      \item
        \type{source/ParticleRenderer.hpp} --- particle tracing visualization;

      \item
        \type{source/Camera.hpp} --- camera to project 3D world on screen.
    \stopitemize
  \item
    Computation:

    \startitemize[packed]
      \item
        \type{source/LbmSolver.hpp} --- low-level mediator for interaction with
        OpenCL LBM solver;

      \item
        \type{source/LbmWorker.hpp} --- thread-safe manager for OpenCL LBM
        solver;

      \item
        \type{resource/kernel.cl} --- OpenCL LBM solver;

      \item
        \type{resource/routine.cl} --- helper routines for OpenCL LBM solver.
    \stopitemize
\stopitemize

Scenarios are described {\em procedurally} in OpenCL. Several interesting
scenarios are supplied out of the box:

\startitemize[packed]
  \item
    \type{resource/scenario1.cl} --- lid-driven cavity;

  \item
    \type{resource/scenario2.cl} --- channel;

  \item
    \type{resource/scenario3.cl} --- lid-driven cavity with obstacles;

  \item
    \type{resource/scenario4.cl} --- serpentine channel;

  \item
    \type{resource/scenario5.cl} --- serpentine channel.
\stopitemize

Creation of new custom scenarios is quite straightforward, simply follow the
same approach as for the default ones.

\stopsection

% ------------------------------------------------------------------------------

\useURL
  [url:8]
  [http://en.wikipedia.org/wiki/Qt_(framework)]
  []
  [Qt]

\useURL
  [url:9]
  [http://qt-project.org/doc/qt-4.8/qtcore.html]
  []
  [QtCore]

\useURL
  [url:10]
  [http://qt-project.org/doc/qt-4.8/qtgui.html]
  []
  [QtGui]

\useURL
  [url:11]
  [http://qt-project.org/doc/qt-4.8/qtopengl.html]
  []
  [QtOpenGL]

\useURL
  [url:12]
  [http://en.wikipedia.org/wiki/Boost_(C\%2B\%2B_libraries)]
  []
  [Boost]

\useURL
  [url:13]
  [http://www.boost.org/doc/libs/1_50_0/libs/smart_ptr/smart_ptr.htm]
  []
  [Smart Pointers]

\useURL
  [url:14]
  [http://www.boost.org/doc/libs/1_50_0/doc/html/boost_lexical_cast.html]
  []
  [Lexical_Cast]

\useURL
  [url:15]
  [http://www.boost.org/doc/libs/1_50_0/doc/html/chrono.html]
  []
  [Chrono]

\useURL
  [url:16]
  [http://www.boost.org/doc/libs/1_50_0/libs/system/doc/index.html]
  []
  [System]

\useURL
  [url:17]
  [http://en.wikipedia.org/wiki/Eigen_(C\%2B\%2B_library)]
  []
  [Eigen]

\useURL
  [url:18]
  [http://oglplus.org/]
  []
  [OGLplus]

\useURL
  [url:19]
  [http://en.wikipedia.org/wiki/OpenGL_Extension_Wrangler_Library]
  []
  [GLEW]

\useURL
  [url:20]
  [http://en.wikipedia.org/wiki/OpenGL]
  []
  [OpenGL]

\useURL
  [url:21]
  [http://en.wikipedia.org/wiki/OpenCL]
  []
  [OpenCL]

\useURL
  [url:22]
  [http://www.khronos.org/registry/cl/api/1.1/cl.hpp]
  []
  [OpenCL C++ Bindings]

\useURL
  [url:23]
  [http://en.wikipedia.org/wiki/C\%2B\%2B11]
  []
  [C++11]

\useURL
  [url:24]
  [http://en.wikipedia.org/wiki/GNU_Compiler_Collection]
  []
  [GCC]

\useURL
  [url:25]
  [http://en.wikipedia.org/wiki/MinGW\#MinGW-w64]
  []
  [MinGW-w64]

\useURL
  [url:26]
  [http://en.wikipedia.org/wiki/Clang]
  []
  [Clang]

\subject
  [dependencies]
  {Dependencies}

\thinrule

\startitemize[n,packed][stopper=.]
  \item
    \from[url:8], version \type{4.7.4} or higher.\crlf
     Components:

    \startitemize[packed]
      \item
        \from[url:9];

      \item
        \from[url:10];

      \item
        \from[url:11].
    \stopitemize

  \item
    \from[url:12], version \type{1.50.0} or higher.\crlf
     Components:

    \startitemize[packed]
      \item
        \from[url:13];

      \item
        \from[url:14];

      \item
        \from[url:15];

      \item
        \from[url:16].
    \stopitemize

  \item
    \from[url:17], version \type{3.1.2} or higher;

  \item
    \from[url:18], version \type{0.32.0} or higher;

  \item
    \from[url:19], version \type{1.9.0} or higher;

  \item
    \from[url:20], version \type{4.0} or higher;

  \item
    \from[url:21], version \type{1.1} exactly;

  \item
    \from[url:22], version \type{1.1} exactly;

  \item
    \from[url:23] compliant compiler;\crlf
    \from[url:24] (\from[url:25] for Windows) or \from[url:26] are recommended.
\stopitemize

\stopsection

% ------------------------------------------------------------------------------

\useURL
  [url:8]
  [http://github.com/martine/ninja]
  []
  [Ninja]

\useURL
  [url:9]
  [http://github.com/martine/ninja]
  []
  [Ninja]

\subject
  [installation]
  {Installation}

\thinrule

{\bf NOTE:} The \type{<...>} notation is used to denote placeholders for your
custom values and paths on your system; feel free to tweak them as you like.

\startitemize[n][stopper=.]
  \item
    Obtain the source code:

    \starttyping
    git clone https://bitbucket.org/Alexander-Shukaev/lattice-boltzmann-method.git <lbm-dir>
    \stoptyping

  \item
    Go to \type{<lbm-dir>}:

    \starttyping
    cd <lbm-dir>
    \stoptyping

  \item
    Configure the build system:

    \starttyping
    export Boost_INCLUDE_DIR=<boost-include-dir>
    export Boost_LIBRARY_DIR=<boost-library-dir>
    export Eigen_INCLUDE_DIR=<eigen-include-dir>
    export OGLplus_INCLUDE_DIR=<oglplus-include-dir>
    export GLEW_INCLUDE_DIR=<glew-include-dir>
    export GLEW_LIBRARY_DIR=<glew-library-dir>
    export OpenCL_INCLUDE_DIR=<opencl-include-dir>
    export OpenCL_LIBRARY_DIR=<opencl-library-dir>
    cmake/configure.py
    \stoptyping

    {\bf NOTE:} By default \type{cmake/configure.py} uses \from[url:8]
    generator. To change that, use the \type{-generator} option, for example:

    \starttyping
    cmake/configure.py -generator="MSYS Makefiles"
    \stoptyping

    {\bf NOTE:} By default \type{cmake/configure.py} configures the
    \type{Release} variant. To change that, use the \type{-variant} option, for
    example:

    \starttyping
    cmake/configure.py -variant=Debug
    \stoptyping

  \item
    Build:

    \starttyping
    cmake/build.py
    \stoptyping

    {\bf NOTE:} By default \type{cmake/build.py} uses \from[url:9] builder. To
    change that, use the \type{-builder} option, for example:

    \starttyping
    cmake/build.py -builder=make
    \stoptyping

    {\bf NOTE:} By default \type{cmake/build.py} builds the \type{Release}
    variant. To change that, use the \type{-variant} option, for example:

    \starttyping
    cmake/build.py -variant=Debug
    \stoptyping

  \item
    Install:

    \starttyping
    cmake/install.py
    \stoptyping

    {\bf NOTE:} By default \type{cmake/install.py} installs to
    \type{<lbm-dir>/install}. To change that, specify the \type{-prefix} option
    to \type{cmake/configure.py}, for example:

    \starttyping
    cmake/configure.py -prefix=<lbm-installation-dir>
    \stoptyping

    {\bf NOTE:} By default \type{cmake/install.py} installs the \type{Release}
    variant. To change that, use the \type{-variant} option, for example:

    \starttyping
    cmake/install.py -variant=Debug
    \stoptyping
\stopitemize

\stopsection

% ------------------------------------------------------------------------------

\subject
  [screenshots]
  {Screenshots}

\thinrule

\switchtobodyfont
  [small]
\placefigure
  [force]
  {Lid-Driven Cavity \#1}{
    \startalignment
      [middle]
    \externalfigure
      [../resource/screenshots/1.png]
      [width=15cm]
    \stopalignment
  }

\placefigure
  [force]
  {Lid-Driven Cavity \#2}{
    \startalignment
      [middle]
    \externalfigure
      [../resource/screenshots/2.png]
      [width=15cm]
    \stopalignment
  }

\placefigure
  [force]
  {Lid-Driven Cavity \#3}{
    \startalignment
      [middle]
    \externalfigure
      [../resource/screenshots/3.png]
      [width=15cm]
    \stopalignment
  }

\placefigure
  [force]
  {Lid-Driven Cavity \#4}{
    \startalignment
      [middle]
    \externalfigure
      [../resource/screenshots/4.png]
      [width=15cm]
    \stopalignment
  }

\placefigure
  [force]
  {Lid-Driven Cavity \#5}{
    \startalignment
      [middle]
    \externalfigure
      [../resource/screenshots/5.png]
      [width=15cm]
    \stopalignment
  }

\placefigure
  [force]
  {Lid-Driven Cavity \#6}{
    \startalignment
      [middle]
    \externalfigure
      [../resource/screenshots/6.png]
      [width=15cm]
    \stopalignment
  }

\placefigure
  [force]
  {Lid-Driven Cavity \#7}{
    \startalignment
      [middle]
    \externalfigure
      [../resource/screenshots/7.png]
      [width=15cm]
    \stopalignment
  }

\placefigure
  [force]
  {Serpentine Channel \#1}{
    \startalignment
      [middle]
    \externalfigure
      [../resource/screenshots/8.png]
      [width=15cm]
    \stopalignment
  }

\placefigure
  [force]
  {Serpentine Channel \#2}{
    \startalignment
      [middle]
    \externalfigure
      [../resource/screenshots/9.png]
      [width=15cm]
    \stopalignment
  }

\placefigure
  [force]
  {Serpentine Channel \#3}{
    \startalignment
      [middle]
    \externalfigure
      [../resource/screenshots/10.png]
      [width=15cm]
    \stopalignment
  }
\switchtobodyfont
  [global]

\stopsection

% ------------------------------------------------------------------------------

\useURL
  [url:10]
  [http://www.gnu.org/licenses/]
  []
  [\hyphenatedurl{http://www.gnu.org/licenses/}]

\page[yes]

\subject
  [copyright]
  {Copyright}

\thinrule

Copyright (C) 2013, Alexander Shukaev. All rights reserved.                   \\
Copyright (C) 2013, Viacheslav Mikerov. All rights reserved.

\subject
  [license]
  {License}

\thinrule

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see \from[url:10].

\stopsection

\stoptext
