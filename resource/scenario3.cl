#define MOVING_WALL_SCENARIO

__constant double3 WallVelocity = (double3)(0.1, 0.0, 0.1);

void
createScenario(
  __global   ClConfig* config,
  __global char*       flagField,
  int                  id,
  int3                 position) {
  if (position.y == (config->sizeY - 1))
    flagField[id] = MOVING_WALL;
  else {
    if (position.x == 0)
      flagField[id] = NO_SLIP;
    else if (position.x == (config->sizeX - 1))
      flagField[id] = NO_SLIP;
    else if (position.y == 0)
      flagField[id] = NO_SLIP;
    else if (position.z == 0)
      flagField[id] = NO_SLIP;
    else if (position.z == (config->sizeZ - 1))
      flagField[id] = NO_SLIP;
    else
      flagField[id] = FLUID;
  }

  int sizeX = (config->sizeX) / 2;
  int sizeY = (config->sizeY) / 2;
  int sizeZ = (config->sizeZ) / 2;

  if (position.x > sizeX &&
      position.y > sizeY &&
      position.z > sizeZ)
    flagField[id] = NO_SLIP;
}
