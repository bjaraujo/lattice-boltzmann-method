#define INFLOW_SCENARIO

__constant double3 InflowVelocity = (double3)(0.3, 0.0, 0.0);

void
createScenario(
  __global   ClConfig* config,
  __global char*       flagField,
  int                  id,
  int3                 position) {
  int sizeX = (config->sizeX) / 7;
  int sizeY = (config->sizeY) / 2;
  // int sizeZ = (config->sizeZ) / 2;

  if (position.y == 0)
    flagField[id] = NO_SLIP;
  else if (position.y == (config->sizeY - 1))
    flagField[id] = NO_SLIP;
  else if (position.z == 0)
    flagField[id] = NO_SLIP;
  else if (position.z == (config->sizeZ - 1))
    flagField[id] = NO_SLIP;
  else if (position.x == 0) {
    if (position.y > sizeY)
      flagField[id] = INFLOW;
    else
      flagField[id] = NO_SLIP;
  } else if (position.x == (config->sizeX - 1)) {
    if (position.y < sizeY)
      flagField[id] = OUTFLOW;
    else
      flagField[id] = NO_SLIP;
  } else {
    flagField[id] = NO_SLIP;

    if (position.x < sizeX) {
      if (position.y > sizeY)
        flagField[id] = FLUID;
    } else if (position.x < 2 * sizeX)
      flagField[id] = FLUID;
    else if (position.x < 3 * sizeX) {
      if (position.y < sizeY)
        flagField[id] = FLUID;
    } else if (position.x < 4 * sizeX)
      flagField[id] = FLUID;
    else if (position.x < 5 * sizeX) {
      if (position.y > sizeY)
        flagField[id] = FLUID;
    } else if (position.x < 6 * sizeX)
      flagField[id] = FLUID;
    else if (position.x < config->sizeX)
      if (position.y < sizeY)
        flagField[id] = FLUID;

  }
}
