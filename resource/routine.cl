#pragma OPENCL EXTENSION cl_khr_fp64 : enable
// #pragma OPENCL EXTENSION all : enable
//

#define Q 19

typedef struct {
  double3 LATTICEVELOCITIES[Q];
  double  LATTICEWEIGHTS[Q];
} LBMDef;

typedef struct {
  unsigned int sizeX;
  unsigned int sizeY;
  unsigned int sizeZ;
  unsigned int resolution;
  double       tau;
  double       wallVelocity;
  LBMDef       lbmDef;
} ClConfig;

// constant double C_S =
//  1.0 / 1.732050807568877193176604123436845839023590087890625;

#define FLUID       0
#define NO_SLIP     1
#define MOVING_WALL 2
#define INFLOW      3
#define OUTFLOW     4

#define CELL_INDEX3(position, sizeX, sizeY) \
  ((position.x) + (position.y) * (sizeX) + (position.z) * (sizeX) * (sizeY))

// #define C_S_2 \
//  (C_S * C_S)
//
// #define C_S_4 \
//  (C_S_2 * C_S_2)

void
computeDensity(__global double* currentCell,
               double*          density) {
  (*density) = currentCell[0];

  for (int i = 1; i < Q; ++i)
    (*density) += currentCell[i];
}

void
computeVelocity(__global double*   currentCell,
                double             density,
                double3*           velocity,
                __global   LBMDef* lbmDef) {
  //  if (density == 0.0) {
  //    *velocity = (double3)(0.0, 0.0, 0.0);
  //    return;
  //  }

  *velocity = currentCell[0] * lbmDef->LATTICEVELOCITIES[0];

  for (int i = 1; i < Q; ++i)
    *velocity += currentCell[i] * lbmDef->LATTICEVELOCITIES[i];

  *velocity /=  density;
}

void
computeFeq(double            density,
           double3           velocity,
           double*           feq,
           __global  LBMDef* lbmDef) {
  double cs  = 1.0 / sqrt(3.0);
  double cs2 = cs * cs;
  double cs4 = 2.0 * cs2 * cs2;

  for (int i = 0; i < Q; ++i) {
    float dotCU = dot(lbmDef->LATTICEVELOCITIES[i], velocity);

    feq[i] = lbmDef->LATTICEWEIGHTS[i] *
             density *
             (1.0 +
              dotCU / cs2 +
              dotCU * dotCU / cs4 -
              dot(velocity, velocity) / (2.0 * cs2));
  }
}

void
computePostCollisionDistributions(__global double* currentCell,
                                  double           tau,
                                  double*          feq) {
  for (int i = 0; i < Q; ++i)
    currentCell[i] -= (currentCell[i] - feq[i]) / tau;
}

int
int3ComparisonResult(
  int3 vector) {
  return vector.x & vector.y & vector.z;
}

bool
isDomain(int3         position,
         unsigned int sizeX,
         unsigned int sizeY,
         unsigned int sizeZ) {
  return position.x >= 0 &&
         position.y >= 0 &&
         position.z >= 0 &&
         position.x <= (sizeX - 1) &&
         position.y <= (sizeY - 1) &&
         position.z <= (sizeZ - 1);
}

bool
isFluidCellIndex3(__global char* flagField,
                  int3           position,
                  unsigned int   id,
                  unsigned int   sizeX,
                  unsigned int   sizeY,
                  unsigned int   sizeZ) {
  // return int3ComparisonResult(position >= 0)  &&
  //        int3ComparisonResult((convert_uint3(position)) <= (size - 1)) &&
  //        (flagField[id] == FLUID);
  return isDomain(position, sizeX, sizeY, sizeZ) &&
         (flagField[id] == FLUID);
}

void
computePosition(unsigned int id,
                unsigned int sizeX,
                unsigned int sizeY,
                int3*        position) {
  unsigned int tempId = id;
  (*position).x = tempId % sizeX;
  tempId        = ((tempId - (*position).x) / sizeX);
  (*position).y = tempId % sizeY;
  tempId        = ((tempId - (*position).y) / sizeY);
  (*position).z = tempId;
}
