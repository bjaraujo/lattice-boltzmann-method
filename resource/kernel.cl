__kernel void
fillFlags(
  __global   ClConfig* config,
  __global char*       flagField,
  __global double*     streamField,
  __global double*     collideField,
  __global float*      densityField,
  __global float*      velocityField) {
  unsigned int id = get_global_id(0);

  if (id >= config->resolution)
    return;

  __global double* streamCell  = streamField + id * Q;
  __global double* collideCell = collideField + id * Q;

  int3 position;
  computePosition(id,
                  config->sizeX,
                  config->sizeY,
                  &position);

  for (int i = 0; i < Q; ++i) {
    int3 newPosition = position + convert_int3(
      config->lbmDef.LATTICEVELOCITIES[i]);

    unsigned int index = CELL_INDEX3(newPosition,
                                     config->sizeX,
                                     config->sizeY);

    if (isDomain(newPosition,
                 config->sizeX,
                 config->sizeY,
                 config->sizeZ)) {
      streamCell[i]  = config->lbmDef.LATTICEWEIGHTS[i];
      collideCell[i] = config->lbmDef.LATTICEWEIGHTS[i];
    } else {
      streamCell[i]  = 0.0;
      collideCell[i] = 0.0;
    }
  }

  double  density;
  double3 velocity;

  computeDensity(collideCell, &density);
  computeVelocity(collideCell, density, &velocity, &config->lbmDef);

  densityField[id]          = density;
  velocityField[id * 4 + 0] = velocity.x;
  velocityField[id * 4 + 1] = velocity.y;
  velocityField[id * 4 + 2] = velocity.z;

  createScenario(config, flagField, id, position);
}

__kernel void
treatBoundary(
  __global   ClConfig* config,
  __global char*       flagField,
  __global double*     collideField) {
  unsigned int id = get_global_id(0);

  if (id >= config->resolution)
    return;

  if (flagField[id] == FLUID)
    return;

  __global double* boundaryCell = collideField + id * Q;

  int3 position;
  computePosition(id,
                  config->sizeX,
                  config->sizeY,
                  &position);

  double cs  = 1.0 / sqrt(3.0);
  double cs2 = cs * cs;

  for (int i = 0; i < Q; ++i) {
    int3 newPosition = position + convert_int3(
      config->lbmDef.LATTICEVELOCITIES[i]);

    unsigned int index = CELL_INDEX3(newPosition,
                                     config->sizeX,
                                     config->sizeY);

    if (!isFluidCellIndex3(flagField,
                           newPosition,
                           index,
                           config->sizeX,
                           config->sizeY,
                           config->sizeZ))
      continue;

    __global double* fluidCell = collideField + index * Q;

    switch (flagField[id]) {
    case NO_SLIP:
      boundaryCell[i] = fluidCell[Q - 1 - i];
      break;

#ifdef MOVING_WALL_SCENARIO
    case MOVING_WALL: {
      double density;
      computeDensity(fluidCell, &density);

      boundaryCell[i] = fluidCell[Q - 1 - i] +
                        (2.0 * config->lbmDef.LATTICEWEIGHTS[i] * density *
                         (dot(config->lbmDef.LATTICEVELOCITIES[i],
                              WallVelocity))) / cs2;
      break;
    }
#endif // MOVING_WALL_SCENARIO

    case OUTFLOW: {
      double  density;
      double3 velocity;
      double  feq[Q];
      computeDensity(fluidCell, &density);
      computeVelocity(fluidCell, density, &velocity, &config->lbmDef);
      computeFeq(1.0, velocity, feq, &config->lbmDef);
      boundaryCell[i] = feq[Q - 1 - i] +
                        feq[i] -
                        fluidCell[Q - 1 - i];
      break;
    }

#ifdef INFLOW_SCENARIO
    case INFLOW: {
      double  feq[Q];
      computeFeq(1.0, InflowVelocity, feq, &config->lbmDef);
      boundaryCell[i] = feq[i];
      break;
    }
#endif // INFLOW_SCENARIO

    default:
      return;
    }
  }
}

__kernel void
doStreaming(
  __global   ClConfig* config,
  __global char*       flagField,
  __global double*     streamField,
  __global double*     collideField) {
  unsigned int id = get_global_id(0);

  if (id >= config->resolution)
    return;

  if (flagField[id] != FLUID)
    return;

  __global double* streamCell = streamField + id * Q;

  int3 position;
  computePosition(id,
                  config->sizeX,
                  config->sizeY,
                  &position);

  for (int i = 0; i < Q; ++i) {
    int3 newPosition = position - convert_int3(
      config->lbmDef.LATTICEVELOCITIES[i]);
    unsigned int index = CELL_INDEX3(newPosition,
                                     config->sizeX,
                                     config->sizeY);
    __global double* collideCell = collideField + index * Q;

    streamCell[i] = collideCell[i];
  }
}

__kernel void
doCollision(
  __global  ClConfig* config,
  __global char*      flagField,
  __global double*    collideField,
  __global float*     densityField,
  __global float*     velocityField) {
  int id = get_global_id(0);

  if (id >= config->resolution)
    return;

  __global double* currentCell = collideField + id * Q;

  double  density;
  double3 velocity;
  computeDensity(currentCell, &density);
  computeVelocity(currentCell, density, &velocity, &config->lbmDef);

  densityField[id]          = density;
  velocityField[id * 4 + 0] = velocity.x;
  velocityField[id * 4 + 1] = velocity.y;
  velocityField[id * 4 + 2] = velocity.z;

  if (flagField[id] != FLUID)
    return;

  double feq[Q];

  computeFeq(density, velocity, feq, &config->lbmDef);
  computePostCollisionDistributions(currentCell, config->tau, feq);
}
