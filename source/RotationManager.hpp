#ifndef RotationManager_hpp
#define RotationManager_hpp

#include "Direction.hpp"
#include "Quaternion.hpp"
#include "Vector.hpp"

#include <QtCore/QDebug>

template <class T, Handedness handedness = Handedness::right>
class RotationManager {
  using Direction  = Math::Direction<T, handedness>;
  using Quaternion = Math::Quaternion<T>;
  using Vector3    = Math::Vector3<T>;

public:
  RotationManager(): _rotation(Quaternion::Identity()), _active(false)
  {}

  RotationManager(RotationManager const& other) = delete;

  Quaternion
  rotation() {
    auto r = _rotation;

    reset();

    return r;
  }

  void
  start() {
    if (_active)
      return;

    _active   = true;
    _rotation = Quaternion::Identity();
  }

  void
  advance(int x, int y, int width, int height, T fovY, T nearZ) {
    if (!_active)
      return;

    auto aspectRatio = T(width) / T(height);
    auto tanY        = std::tan(fovY / T(2));
    auto tanX        = tanY * aspectRatio;

    auto halfNearHeight = tanY * nearZ;
    auto halfNearWidth  = tanX * nearZ;

    auto nearY = halfNearHeight * (T(1) - 2 * T(y) / T(height));
    auto nearX = halfNearWidth *  (T(1) - 2 * T(x) / T(width));

    Vector3 from = Direction::forward();
    Vector3 to   = nearZ * Direction::forward() +
                   nearY * Direction::up() +
                   nearX * Direction::left();

    _rotation = _rotation * Quaternion::FromTwoVectors(from, to);

    // qDebug() << _rotation.x()
    // << _rotation.y()
    // << _rotation.z()
    // << _rotation.w();
  }

  void
  stop() {
    if (!_active)
      return;

    _active = false;
  }

  void
  reset()
  { _rotation = Quaternion::Identity(); }

private:
  Quaternion _rotation;
  bool       _active;
};

using RotationManagerd = RotationManager<double>;
using RotationManagerf = RotationManager<float>;

#endif