#ifndef Row_hpp
#define Row_hpp

#include <Eigen/Geometry>

namespace Math {
template <class T, int size>
using Row = Eigen::Matrix<T, 1, size>;

template <class T>
using Row3 = Row<T, 3>;

template <class T>
using Row4 = Row<T, 4>;
}

#endif