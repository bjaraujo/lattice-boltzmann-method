#include "IterationCounter.hpp"
// -----------------------------------------------------------------------------

IterationCounter::
IterationCounter(): _iterationCount(0), _iterationsPerSecond(0)
{ _stopwatch.start(); }

int
IterationCounter::
iterationsPerSecond() const
{ return _iterationsPerSecond; }

void
IterationCounter::
update() {
  _iterationCount++;

  if (_stopwatch.hasExpired(boost::chrono::seconds(1))) {
    _stopwatch.restart();

    _iterationsPerSecond = _iterationCount;
    _iterationCount      = 0;
  }
}