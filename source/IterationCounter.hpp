#ifndef IterationCounter_hpp
#define IterationCounter_hpp

#include "Stopwatch.hpp"

class IterationCounter {
public:
  IterationCounter();

  int
  iterationsPerSecond() const;

  void
  update();

private:
  int _iterationCount;
  int _iterationsPerSecond;

  HighResolutionStopwatch _stopwatch;
};

#endif