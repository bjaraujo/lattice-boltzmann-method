#ifndef VelocityRenderer_hpp
#define VelocityRenderer_hpp

#include "Matrix.hpp"

#include <GL/glew.h>

#include <oglplus/all.hpp>

class VectorField;

class VelocityRenderer {
public:
  VelocityRenderer(VectorField const* velocityField);

  GLuint
  sampleCountX() const;

  GLuint
  sampleCountY() const;

  GLuint
  sampleCountZ() const;

  void
  render(Math::Matrix4x4f const& mvp, GLfloat scale) const;

private:
  void
  initializeProgram();

  void
  initializeGlyph();

  oglplus::Context _gl;

  oglplus::Program _program;

  oglplus::VertexArray _vertexArray;

  oglplus::Buffer _positionBuffer;
  oglplus::Buffer _shadeBuffer;
  oglplus::Buffer _indexBuffer;

  GLuint _instanceCount;

  VectorField const* _velocityField;
};

#endif