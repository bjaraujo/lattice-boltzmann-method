#ifndef Core_LbmSolver_hpp
#define Core_LbmSolver_hpp

#include "CLInitialization.hpp"
#include "LoggingMacros.hpp"
#include "Source.hpp"

#include "LbmDefinition.hpp"

struct LBMDef {
  cl_double3 LATTICEVELOCITIES[Q];
  cl_double  LATTICEWEIGHTS[Q];
};

struct Config {
  cl_uint   sizeX;
  cl_uint   sizeY;
  cl_uint   sizeZ;
  cl_uint   resolution;
  cl_double tau;
  cl_double wallVelocity;
  LBMDef    lbmDef;
};

class LbmSolver {
  typedef std::vector<std::string> IncludeFilePathList;
  typedef std::vector<Source>      IncludeSourceList;

public:
  LbmSolver();

  LbmSolver(LbmSolver const& other);

  virtual
  ~LbmSolver();

  LbmSolver const&
  operator=(LbmSolver const& other);

  void
  clInitialization(
    CL::CLInitialization::Shared const& sharedClInitialization) {
    _sharedClInitialization = sharedClInitialization;
  }

  void
  kernelSource(std::string const& sourceFileName);

  void
  routineSource(std::string const& sourceFileName);

  void
  includeFiles(std::string const& includeFilePath);

  void
  includeFiles(IncludeFilePathList const& includeFilePathList);

  void
  sizeX(unsigned int const& sizeX) { _sizeX = sizeX + 2; }

  unsigned int
  sizeX() const { return _sizeX - 2; }

  void
  sizeY(unsigned int const& sizeY) { _sizeY = sizeY + 2; }

  unsigned int
  sizeY() const { return _sizeY - 2; }

  void
  sizeZ(unsigned int const& sizeZ) { _sizeZ = sizeZ + 2; }

  unsigned int
  sizeZ() const { return _sizeZ - 2; }

  void
  tau(float const& tau) { _tau = tau; }

  void
  wallVelocity(float const& wallVelocity) { _wallVeclocity = wallVelocity; }

  void
  initialize();

private:
  void
  initLbmDef();

public:
  void
  setSize();

  void
  setTau();

  void
  setWallVelocity();

  void
  setDensityBuffer(GLenum target,
                   GLuint textureObject);

  void
  updateDensityBuffer();

  void
  setVelocityBuffer(GLenum target,
                    GLuint textureObject);

  void
  updateVelocityBuffer();

  void
  setFlagBuffer(GLenum target,
                GLuint textureObject);

  void
  updateFlagBuffer();

  void
  updateConfig();

  void
  updateFlags();

  void
  run();

private:
  void
  swapBuffers();

public:
  void
  setBuffers();

public:
  void
  release();

private:
  Source                       _routineSource;
  Source                       _kernelSource;
  IncludeSourceList            _includeSourceList;
  CL::CLInitialization::Shared _sharedClInitialization;
  Config                       _config;
  unsigned int                 _sizeX;
  unsigned int                 _sizeY;
  unsigned int                 _sizeZ;
  float                        _tau;
  float                        _wallVeclocity;

  CL::CommandQueue _queue;
  CL::Program      _program;
  CL::Kernel       _fillFlagsKernel;
  CL::Kernel       _doStreamingKernel;
  CL::Kernel       _doCollisionKernel;
  CL::Kernel       _treatBoundaryKernel;
  CL::Image3DGL    _densityImage;
  CL::Image3DGL    _velocityImage;
  CL::Image3DGL    _flagImage;
  CL::Buffer       _configBuffer;
  CL::Buffer       _flagFieldBuffer;
  CL::Buffer       _streamFieldBuffer;
  CL::Buffer       _collideFieldBuffer;
  CL::Buffer       _densityBuffer;
  CL::Buffer       _velocityBuffer;
  CL::NDRange      _global;
  CL::NDRange      _local;

  float*                  _solution;
  std::vector<CL::Memory> _glObjects;
  bool                    _swap;
};   /* ----- end of LbmSolver ----- */

#endif /* ----- end Core_LbmSolver_hpp ----- */
