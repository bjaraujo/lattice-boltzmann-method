#ifndef AngleAxis_hpp
#define AngleAxis_hpp

#include <Eigen/Geometry>

namespace Math {
template <class T>
using AngleAxis = Eigen::AngleAxis<T>;
}

#endif