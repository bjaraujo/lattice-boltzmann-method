#include "DensityRenderer.hpp"

#include "ScalarField.hpp"

#include <oglplus/bound/buffer.hpp>

#include <QtCore/QDebug>

#include <vector>
// -----------------------------------------------------------------------------

using Math::Matrix4x4f;

using oglplus::BlendFunction;
using oglplus::Buffer;
using oglplus::Capability;
using oglplus::DataType;
using oglplus::FragmentShader;
using oglplus::PrimitiveType;
using oglplus::Program;
using oglplus::Uniform;
using oglplus::UniformSampler;
using oglplus::VertexArray;
using oglplus::VertexShader;

using std::vector;

DensityRenderer::
DensityRenderer(ScalarField const* densityField): _densityField(densityField) {
  assert(vertexCountX() >= 2);
  assert(vertexCountY() >= 2);
  assert(vertexCountZ() >= 2);

  _vertexCountZY = vertexCountZ() * vertexCountY();
  _vertexCountXZ = vertexCountX() * vertexCountZ();
  _vertexCountXY = vertexCountX() * vertexCountY();

  _indexCountZY = (vertexCountZ() * 2) * (vertexCountY() - 1) +
                  (vertexCountY() - 2) * 2;
  _indexCountXZ = (vertexCountX() * 2) * (vertexCountZ() - 1) +
                  (vertexCountZ() - 2) * 2;
  _indexCountXY = (vertexCountX() * 2) * (vertexCountY() - 1) +
                  (vertexCountY() - 2) * 2;

  initializeProgram();
  initializeZY();
  initializeXZ();
  initializeXY();
}

GLuint
DensityRenderer::
vertexCountX() const
{ return _densityField->sampleCountX() + 1; }

GLuint
DensityRenderer::
vertexCountY() const
{ return _densityField->sampleCountY() + 1; }

GLuint
DensityRenderer::
vertexCountZ() const
{ return _densityField->sampleCountZ() + 1; }

void
DensityRenderer::
initializeProgram() {
  VertexShader   vs;
  FragmentShader fs;

  vs.Source(
    "#version 400\n"

    "uniform sampler3D _densitySampler;"

    "subroutine vec3 PlanePosition();"
    "subroutine uniform PlanePosition _planePosition;"

    "uniform mat4 _mvp;"

    "uniform vec3 _densityPositionMax;"

    "uniform float _densityMax = 1.2;"
    "uniform float _densityMin = 0.8;"

    "uniform uvec3 _texelMaxIndex;"

    "in vec2 _position;"

    "out vec4 v_color;"

    "subroutine (PlanePosition)\n"
    "vec3 "
    "positionZY() {"
    "  return vec3(gl_InstanceID + 0.5, _position.y, _position.x);"
    "}"

    "subroutine (PlanePosition)\n"
    "vec3 "
    "positionXZ() {"
    "  return vec3(_position.x, gl_InstanceID + 0.5, _position.y);"
    "}"

    "subroutine (PlanePosition)\n"
    "vec3 "
    "positionXY() {"
    "  return vec3(_position.x, _position.y, gl_InstanceID + 0.5);"
    "}"

    "vec4 toColor(float density) {"
    "  float k = (density - _densityMin) / (_densityMax - _densityMin);"

    "  return vec4(-2 * k * k + 3 * k, -4 * k * k + 4 * k, -2 * k * k + k + 1, 4 * k * k - 4 * k + 1);"
    "}"

    "void "
    "main() {"
    "  vec3 position = _planePosition();"

    "  vec3 densityPosition = clamp(position, vec3(1, 1, 1), _densityPositionMax);"

    "  float density = texture(_densitySampler, densityPosition / _texelMaxIndex).r;"

    "  v_color = toColor(density);"

    "  gl_Position = _mvp * vec4(position, 1);"
    "}").Compile();

  fs.Source(
    "#version 400\n"

    "uniform float _alpha = 0.1;"

    "in vec4 v_color;"

    "out vec4 f_color;"

    "void "
    "main() {"
    "  f_color = vec4(v_color.rgb, v_color.a * _alpha);"
    "}").Compile();

  _program.AttachShader(vs);
  _program.AttachShader(fs);

  _program.Link();

  _program.DetachShader(vs);
  _program.DetachShader(fs);

  auto programName = Expose(_program).Name();

  _subroutineZY = glGetSubroutineIndex(programName,
                                       GL_VERTEX_SHADER,
                                       "positionZY");
  _subroutineXZ = glGetSubroutineIndex(programName,
                                       GL_VERTEX_SHADER,
                                       "positionXZ");
  _subroutineXY = glGetSubroutineIndex(programName,
                                       GL_VERTEX_SHADER,
                                       "positionXY");
  _program.Use();

  UniformSampler(_program, "_densitySampler").Set(0);
  Uniform<GLuint>(_program, "_texelMaxIndex").SetVector(
    _densityField->texelCountX() - 1,
    _densityField->texelCountY() - 1,
    _densityField->texelCountZ() - 1);

  Uniform<GLfloat>(_program, "_densityPositionMax").SetVector(
    _densityField->sampleCountX() - 1.0f,
    _densityField->sampleCountY() - 1.0f,
    _densityField->sampleCountZ() - 1.0f);

  Program::UseNone();
}

void
DensityRenderer::
initializeZY() {
  _vertexArrayZY.Bind();
  {
    auto b = Bind(_vertexBufferZY, Buffer::Target::Array);

    vector<GLfloat> data;

    data.reserve(2 * _vertexCountZY);

    for (int y = 0; y < vertexCountY(); ++y)
      for (int z = 0; z < vertexCountZ(); ++z) {
        data.push_back(z + 0.5f);
        data.push_back(y + 0.5f);
      }

    b.Data(data);

    (_program | "_position").Setup<GLfloat>(2).Enable();
  }

  {
    auto b = Bind(_indexBufferZY, Buffer::Target::ElementArray);

    vector<GLuint> data;

    data.reserve(_indexCountZY);

    for (int y = 0; y < vertexCountY() - 1; ++y)
      for (int z = 0; z < vertexCountZ(); ++z) {
        if (z == 0 && y > 0)
          data.push_back(z + y * vertexCountZ());

        data.push_back(z + y * vertexCountZ());
        data.push_back(z + (y + 1) * vertexCountZ());

        if (z == vertexCountZ() - 1 && y < vertexCountY() - 2)
          data.push_back(z + (y + 1) * vertexCountZ());
      }

    b.Data(data);
  }

  VertexArray::Unbind();
}

void
DensityRenderer::
initializeXZ() {
  _vertexArrayXZ.Bind();
  {
    auto b = Bind(_vertexBufferXZ, Buffer::Target::Array);

    vector<GLfloat> data;

    data.reserve(2 * _vertexCountXZ);

    for (int z = 0; z < vertexCountZ(); ++z)
      for (int x = 0; x < vertexCountX(); ++x) {
        data.push_back(x + 0.5f);
        data.push_back(z + 0.5f);
      }

    b.Data(data);

    (_program | "_position").Setup<GLfloat>(2).Enable();
  }

  {
    auto b = Bind(_indexBufferXZ, Buffer::Target::ElementArray);

    vector<GLuint> data;

    data.reserve(_indexCountXZ);

    for (int z = 0; z < vertexCountZ() - 1; ++z)
      for (int x = 0; x < vertexCountX(); ++x) {
        if (x == 0 && z > 0)
          data.push_back(x + z * vertexCountX());

        data.push_back(x + z * vertexCountX());
        data.push_back(x + (z + 1) * vertexCountX());

        if (x == vertexCountX() - 1 && z < vertexCountZ() - 2)
          data.push_back(x + (z + 1) * vertexCountX());
      }

    b.Data(data);
  }

  VertexArray::Unbind();
}

void
DensityRenderer::
initializeXY() {
  _vertexArrayXY.Bind();
  {
    auto b = Bind(_vertexBufferXY, Buffer::Target::Array);

    vector<GLfloat> data;

    data.reserve(2 * _vertexCountXY);

    for (int y = 0; y < vertexCountY(); ++y)
      for (int x = 0; x < vertexCountX(); ++x) {
        data.push_back(x + 0.5f);
        data.push_back(y + 0.5f);
      }

    b.Data(data);

    (_program | "_position").Setup<GLfloat>(2).Enable();
  }

  {
    auto b = Bind(_indexBufferXY, Buffer::Target::ElementArray);

    vector<GLuint> data;

    data.reserve(_indexCountXY);

    for (int y = 0; y < vertexCountY() - 1; ++y)
      for (int x = 0; x < vertexCountX(); ++x) {
        if (x == 0 && y > 0)
          data.push_back(x + y * vertexCountX());

        data.push_back(x + y * vertexCountX());
        data.push_back(x + (y + 1) * vertexCountX());

        if (x == vertexCountX() - 1 && y < vertexCountY() - 2)
          data.push_back(x + (y + 1) * vertexCountX());
      }

    b.Data(data);
  }

  VertexArray::Unbind();
}

void
DensityRenderer::
renderZY() const {
  glUniformSubroutinesuiv(GL_VERTEX_SHADER, 1, &_subroutineZY);

  _vertexArrayZY.Bind();
  _gl.DrawElementsInstanced(PrimitiveType::TriangleStrip,
                            _indexCountZY,
                            DataType::UnsignedInt,
                            vertexCountX());
  VertexArray::Unbind();
}

void
DensityRenderer::
renderXZ() const {
  glUniformSubroutinesuiv(GL_VERTEX_SHADER, 1, &_subroutineXZ);

  _vertexArrayXZ.Bind();
  _gl.DrawElementsInstanced(PrimitiveType::TriangleStrip,
                            _indexCountXZ,
                            DataType::UnsignedInt,
                            vertexCountY());
  VertexArray::Unbind();
}

void
DensityRenderer::
renderXY() const {
  glUniformSubroutinesuiv(GL_VERTEX_SHADER, 1, &_subroutineXY);

  _vertexArrayXY.Bind();
  _gl.DrawElementsInstanced(PrimitiveType::TriangleStrip,
                            _indexCountXY,
                            DataType::UnsignedInt,
                            vertexCountZ());
  VertexArray::Unbind();
}

void
DensityRenderer::
render(Matrix4x4f const& mvp, GLfloat alpha) const {
  auto cullFace  = _gl.IsEnabled(Capability::CullFace);
  auto depthTest = _gl.IsEnabled(Capability::DepthTest);
  auto blend     = _gl.IsEnabled(Capability::Blend);

  _gl.Disable(Capability::CullFace);
  _gl.Disable(Capability::DepthTest);
  _gl.Enable(Capability::Blend);
  // _gl.BlendFunc(BlendFunction::SrcAlpha, BlendFunction::OneMinusSrcAlpha);
  _gl.BlendFunc(BlendFunction::SrcAlpha, BlendFunction::One);
  // _gl.BlendFunc(BlendFunction::Zero, BlendFunction::SrcColor);

  _densityField->attach(0);

  _program.Use();

  Uniform<GLfloat>(_program, "_mvp").SetMatrix<4, 4>(1, mvp.data());
  // Uniform<GLfloat>(_program, "_alpha").Set(alpha);

  renderZY();
  renderXZ();
  renderXY();

  Program::UseNone();

  _densityField->detach(0);

  if (cullFace)
    _gl.Enable(Capability::CullFace);

  if (depthTest)
    _gl.Enable(Capability::DepthTest);

  if (!blend)
    _gl.Disable(Capability::Blend);
}