#include "LbmScenarioFactory.hpp"

#include "LbmScenario.hpp"
LbmScenarioFactory::
LbmScenarioFactory() {}

LbmScenarioFactory::
LbmScenarioFactory(LbmScenarioFactory const& other) {}

LbmScenarioFactory::
~LbmScenarioFactory() {}

LbmScenarioFactory const&
LbmScenarioFactory::
operator=(LbmScenarioFactory const& other) {}

Scenario*
LbmScenarioFactory::
createScenario(QGLWidget* glWidget) {
  return new LbmScenario(glWidget);
}

ScenarioFactory::Shared
LbmScenarioFactory::
shared() {
  return ScenarioFactory::Shared(new LbmScenarioFactory());
}
