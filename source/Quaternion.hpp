#ifndef Quaternion_hpp
#define Quaternion_hpp

#include <Eigen/Geometry>

namespace Math {
template <class T>
using Quaternion = Eigen::Quaternion<T>;

using Quaternionf = Quaternion<float>;
}

#endif