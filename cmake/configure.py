#!/usr/bin/env python
# encoding: utf-8

import inspect
import os
import sys

cmake_dir = os.path.realpath(
    os.path.abspath(
        os.path.split(
            inspect.getfile(
                inspect.currentframe()
            )
        )[0]
    )
)

project_dir = os.path.dirname(cmake_dir)
python_dir = os.path.join(project_dir, 'python')

if python_dir not in sys.path:
    sys.path.insert(0, python_dir)

# Add and parse arguments {{{
# -----------------------------------------------------------------------------
from argparse import ArgumentParser

parser = ArgumentParser()

parser.add_argument('-generator', default='Ninja')
parser.add_argument('-variant', default='Release')
parser.add_argument('-static-linking', default='Off')

install_dir = os.path.join(project_dir, 'install')

parser.add_argument('-prefix', default=install_dir)

args = parser.parse_args()
# -----------------------------------------------------------------------------
# }}}

# Filter arguments {{{
# -----------------------------------------------------------------------------
args.generator = args.generator.strip()
args.variant = args.variant.strip()

from error import EmptyArgumentError

if not args.generator:
    raise EmptyArgumentError('GENERATOR')

if not args.variant:
    raise EmptyArgumentError('VARIANT')
# -----------------------------------------------------------------------------
# }}}

# Prepare CMake arguments {{{
# -----------------------------------------------------------------------------
from cmake import arg

cmake_args = ['cmake',
              '-G', args.generator,
              project_dir,
              arg('CMAKE_BUILD_TYPE', args.variant),
              arg('CMAKE_STATIC_LINKING', args.static_linking)]

if args.prefix:
    cmake_args.append(arg('CMAKE_INSTALL_PREFIX', args.prefix))
# -----------------------------------------------------------------------------
# }}}

# Invoke CMake {{{
# -----------------------------------------------------------------------------
build_dir = os.path.join(project_dir, 'build')

if not os.path.isdir(build_dir):
    os.mkdir(build_dir)

variant_dir = os.path.join(build_dir, args.variant.lower())

import shutil

if os.path.isdir(variant_dir):
    shutil.rmtree(variant_dir)

if not os.path.isdir(variant_dir):
    os.mkdir(variant_dir)

os.chdir(variant_dir)

from process import call

call(cmake_args)
# -----------------------------------------------------------------------------
# }}}
